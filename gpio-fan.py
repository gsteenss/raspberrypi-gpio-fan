#!/usr/bin/python3
import RPi.GPIO as GPIO
import time
import sensors

# default fan is off
fan_is_running=0
switch_fan_state=0
fan_start_temp=60
fan_stop_temp=50
# default temperature poll interval is 5 seconds
poll_interval_time=5

# CrowPi2 or Raspberry pin configurations
crowpi2_relay_pin = 21
raspberrypi_gpio_pin = 14
# set one of the above gpio_pins according to your device or setup
gpio_pin = raspberrypi_gpio_pin

sensors.init()
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.setup(gpio_pin, GPIO.OUT)
# read GPIO pin to see if fan is already running
if GPIO.input(gpio_pin)==True: fan_is_running=1
GPIO.cleanup() 
while True: 
      for chip in sensors.iter_detected_chips():
            for feature in chip:
                if feature.label == "temp1":
                    print('  %s: %.2f' % (feature.label, feature.get_value()))
                    if feature.get_value()>fan_start_temp and fan_is_running==0:
                            switch_fan_state=1
                            fan_is_running=1
                    if feature.get_value()<fan_stop_temp and fan_is_running==1:
                            switch_fan_state=1
                            fan_is_running=0
      print('FAN SWITCH: ', switch_fan_state)
      if switch_fan_state==1:
          GPIO.setmode(GPIO.BCM)
          GPIO.setup(gpio_pin, GPIO.OUT)
          if fan_is_running==1:
              GPIO.output(gpio_pin, GPIO.HIGH)
              print('FAN ON')
              switch_fan_state=0
              fan_is_running=1
          if fan_is_running==0:
              GPIO.output(gpio_pin, GPIO.LOW)
              print("FAN OFF")
              switch_fan_state=0
              fan_is_running=0
      time.sleep(poll_interval_time)
      print('FAN IS RUNNING: ', fan_is_running)

GPIO.cleanup()
sensors.cleanup()

