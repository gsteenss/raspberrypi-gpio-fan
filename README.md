# raspberrypi-gpio-fan

Raspberry Pi project for activating/deactivating a 5V fan using GPIO pins based on CPU temp sensor readings


Usage:


sudo apt install python3-pip python3-rpi.gpio lm-sensors -y
sudo pip3 install pysensors

optionally: install as systemd service


RaspberryPi:

For use with generic Raspberry Pis 3/3+/4
uses a NPN transistor to switch fan on or off, as described here:

https://raspmer.blogspot.com/2019/02/control-cooling-fan-with-gpio-fan.html

Also see refernece images for pinouts & connections.

CrowPi2:

uses the relay included instead, loosely based on:

https://github.com/Elecrow-RD/CrowPi/blob/master/Examples/relay.py
https://www.elecrow.com/download/product/SES14002K/CrowPi_Python_Lessons.pdf


Install systemd service:
sudo cp gpio-fan.py /opt/
chmod +x /opt/gpio-fan.py
sudo cp gpio-fan.service /etc/systemd/system/gpio-fan.service
sudo systemctl daemon-reload
sudo systemctl enable gpio-fan --now
sudo systemctl status gpio-fan
